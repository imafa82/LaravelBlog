@extends('layouts.app')
@section('content')
<main class="container-fluid">
    <div class="row">
        <div class="jumbotron">
            <h1>Modifica Post</h1>
        </div>
        <div class="col-sm-10 col-sm-offset-1">
            {!! Form::model($blog, ['method' => 'POST', 'action' => ['BlogController@update', $blog->id]]) !!}
            {{method_field('PATCH')}}
            <div class="form-group">
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', null, ['class'=> 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('body', 'Body:') !!}
                {!! Form::textarea('body', null, ['class'=> 'form-control']) !!}
            </div>
            <div>
                {!! Form::submit('Edit a Blog', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

            {!! Form::open(['method' => 'DELETE', 'action' => ['BlogController@destroy', $blog->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete Blog', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</main>


@endsection