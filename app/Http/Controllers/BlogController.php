<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(){
        $blogs = Blog::latest()->get();
        return view('blog.index', compact('blogs'));
    }

    public function create(){
        return view('blog.create');
    }

    public function store(Request $request){
        $input = $request->all();
        Blog::create($input);
        return back();
    }
    public function show($id){
        $blog = Blog::findOrfail($id);
        return view('blog.show', compact('blog'));
    }

    public function edit($id){
        $blog = Blog::findOrFail($id);
        return view('blog.edit', compact('blog'));
    }

    public function update(Request $request, Blog $blog){
        $input = $request->all();
        $blog->update($input);
        return back();
    }

    public function destroy(Request $request, Blog $blog){
        $blog->delete();
        return redirect('/blog/bin');
    }

    public function bin(){
        $deletedBlogs = Blog::onlyTrashed()->get();
        return view('blog.bin', compact('deletedBlogs'));
    }

    public function restore($id){
        $blog = Blog::onlyTrashed()->findOrFail($id);
        $blog->restore($blog);

        return redirect('/blog');
    }

    public function destroyBlog($id){
        $blog = Blog::onlyTrashed()->findOrFail($id);
        $blog->forceDelete();
        return redirect('/blog/bin');
    }
}
